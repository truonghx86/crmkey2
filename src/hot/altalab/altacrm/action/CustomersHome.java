package altalab.altacrm.action;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import altalab.altacrm.model.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityHome;
import org.jboss.seam.persistence.PersistenceProvider;

@Name("customersHome")
public class CustomersHome extends EntityHome<Customers> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5579913275931003723L;

	public void setCustomersCustomerId(Integer id) {
		setId(id);
	}

	public Integer getCustomersCustomerId() {
		return (Integer) getId();
	}

	@Override
	protected Customers createInstance() {
		Customers customers = new Customers();
		return customers;
	}

	public void load() {
		if (isIdDefined()) {
			wire();
		}
	}

	public void wire() {
		getInstance();
	}

	public boolean isWired() {
		return true;
	}

	public Customers getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}

	public TblUsers getUserCreate(){
		return getEntityManager().find(TblUsers.class, instance.getRegUser());
	}
	public TblUsers getUserUpdate(){
		return getEntityManager().find(TblUsers.class, instance.getUpdUser());
	}

	@SuppressWarnings("unchecked")
	@Override	
	public String persist(){
//		Calendar dateExpected = Calendar.getInstance();
//		dateExpected.setTime(new Date());
//		// cong so ngay cuoi tuan
//		dateExpected.add(Calendar.DATE, 7);
//
//		if(instance.getDeploymentDate().after(dateExpected.getTime())){
//			addFacesMessageFromResourceBundle("Ngày cài đặt quá xa so với ngày hiện tại", "");
//			return "";
//		}
		List<Customers> listCustomers = getEntityManager().
		createQuery("from Customers where customerCode = '"+instance.getCustomerCode().trim()+"'")
		.getResultList();
		if(listCustomers != null && listCustomers.size() > 0){
			addFacesMessageFromResourceBundle("Mã khách hàng đã tồn tại", "");
			return "";
		}
//		List<Customers> listCustomers2 = getEntityManager().
//		createQuery("from Customers where activeKey = '"+instance.getActiveKey().trim()+"'")
//		.getResultList();
//		if(listCustomers2 != null && listCustomers2.size() > 0){
//			addFacesMessageFromResourceBundle("Active key đã tồn tại. Mã K.H "
//					+listCustomers2.get(0).getCustomerCode()
//					+" tên K.H "+listCustomers2.get(0).getCustomerName()
//					, "");
//			return "";
//		}
		return super.persist();
	}
	@SuppressWarnings("unchecked")
	@Override
	public String update(){
//		Calendar dateExpected = Calendar.getInstance();
//		dateExpected.setTime(new Date());
//		// cong so ngay so voi ngay hien tai
//		dateExpected.add(Calendar.DATE, 7);
//
//		if(instance.getDeploymentDate().after(dateExpected.getTime())){
//			addFacesMessageFromResourceBundle("Ngày cài đặt quá xa so với ngày hiện tại", "");
//			return "";
//		}
//		List<Customers> listCustomers2 = getEntityManager().
//		createQuery("from Customers where activeKey = '"+instance.getActiveKey().trim()+"'"
//				+" customerId != "+instance.getCustomerId())
//		.getResultList();
//		if(listCustomers2 != null && listCustomers2.size() > 0){
//			addFacesMessageFromResourceBundle("Active key đã tồn tại. Mã K.H "
//					+listCustomers2.get(0).getCustomerCode()
//					+" tên K.H "+listCustomers2.get(0).getCustomerName()
//					, "");
//			return "";
//		}
		return super.update();
	}

	public String giahan(){
//		Calendar dateExpected = Calendar.getInstance();
//		dateExpected.setTime(new Date());
//		// cong so ngay so voi ngay hien tai
//		dateExpected.add(Calendar.DATE, 15);
//
//		if(instance.getDeploymentDate().after(dateExpected.getTime())){
//			addFacesMessageFromResourceBundle("Ngày cài đặt quá xa so với ngày hiện tại", "");
//			return "";
//		}
		Integer id = instance.getCustomerId();
		Customers customers = new Customers();
		customers.setActiveKey(instance.getActiveKey());
		customers.setCustomerAddress(instance.getCustomerAddress());
		customers.setCustomerCode(instance.getCustomerCode());
		customers.setCustomerDescription(instance.getCustomerDescription());
		customers.setCustomerName(instance.getCustomerName());
		customers.setDeploymentDate(instance.getDeploymentDate());
		customers.setFax(instance.getFax());
		customers.setFinishDate(instance.getFinishDate());
		customers.setNumberUser(instance.getNumberUser());
		customers.setTaxCode(instance.getTaxCode());
		customers.setTelephone(instance.getTelephone());
		customers.setVersionType(instance.getVersionType());
		customers.setDeploymentDate(instance.getDeploymentDate());
		customers.setFinishDate(instance.getFinishDate());
		customers.setDeploymentDate(instance.getDeploymentDate());
		customers.setTypeCompany(instance.getTypeCompany());
		customers.setDisplayLogo(instance.getDisplayLogo());
		customers.setPossition(instance.getPossition());
		customers.setBankAcount(instance.getBankAcount());
		customers.setBankLocation(instance.getBankLocation());
		customers.setCompanyEmail(instance.getCompanyEmail());
		getEntityManager().clear();
		getEntityManager().persist( customers );
		createdMessage();
		raiseAfterTransactionSuccessEvent();
		getEntityManager().createNativeQuery("update Customers set status = 1 where customer_id = "+ id).executeUpdate();
		getEntityManager().flush();

		instance = customers;
		assignId( PersistenceProvider.instance().getId( getInstance(), getEntityManager() ) );
		return "persisted";
	}

	public void onchangeFinishDate(){
		if(instance != null && instance.getDeploymentDate() != null){
			if(instance.getVersionType() == 0){
				Calendar finishDate = Calendar.getInstance();
				finishDate.setTime(instance.getDeploymentDate());
				finishDate.add(Calendar.YEAR, 1);
				instance.setFinishDate(finishDate.getTime());
			}else if(instance.getVersionType() == 1){
				Calendar finishDate = Calendar.getInstance();
				finishDate.setTime(instance.getDeploymentDate());
				finishDate.add(Calendar.DATE, 15);
				instance.setFinishDate(finishDate.getTime());
			}
		}
	}
	public void giahanFinishDate(){
		getInstance();
		instance.setDeploymentDate(instance.getFinishDate());
		Calendar finishDate = Calendar.getInstance();
		finishDate.setTime(instance.getDeploymentDate());
		finishDate.add(Calendar.YEAR, 1);
		instance.setFinishDate(finishDate.getTime());
	}
	private String sqlAdd;
	private String sqlEdit;

	public String getSqlAdd() {
		sqlAdd = "INSERT company_info (COMPANY_NAME, COMPANY_ADDRESS, COMPANY_EMAIL, COMPANY_PHONE, " +
		"TAX_CODE, REPRESENTATIVE, POSSITION, COMPANY_FAX, BANK_ACOUNT, BANK_LOCATION, COMPANY_WEBSITE, " +
		"IMG, DISPLAY_LOGO) ";
		sqlAdd = sqlAdd + " VALUES('"+
		instance.getCustomerName()+"',"+
		"'"+instance.getCustomerAddress()+"',"+
		"'"+(instance.getCompanyEmail() != null ? instance.getCompanyEmail() : "")+"',"+
		"'"+instance.getTelephone()+"',"+
		"'"+instance.getTaxCode()+"',"+
		"'"+(instance.getRepresentative() != null ? instance.getRepresentative() : "")+"',"+
		"'"+(instance.getPossition() != null ? instance.getPossition() : "")+"',"+
		"'"+instance.getFax()+"',"+
		"'"+(instance.getBankAcount() != null ? instance.getBankAcount() : "")+"',"+
		"'"+(instance.getBankLocation() != null ? instance.getBankLocation() : "")+"',"+
		"'"+(instance.getCompanyWebsite() != null ? instance.getCompanyWebsite() : "")+"',"+
		"'"+(instance.getImg() != null ? instance.getImg() : "")+"',"+
		"'"+instance.getDisplayLogo()+"'"+
		");";
		return sqlAdd;
	}

	public void setSqlAdd(String sqlAdd) {
		this.sqlAdd = sqlAdd;
	}

	public String getSqlEdit() {
		sqlEdit = "UPDATE company_info set ";
		sqlEdit = sqlEdit + " COMPANY_NAME ='"+instance.getCustomerName()+"',";
		sqlEdit = sqlEdit + " COMPANY_ADDRESS ='"+instance.getCustomerAddress()+"',";
		sqlEdit = sqlEdit + " COMPANY_EMAIL ='"+instance.getCompanyEmail()+"',";
		sqlEdit = sqlEdit + " COMPANY_PHONE ='"+instance.getTelephone()+"',";
		sqlEdit = sqlEdit + " TAX_CODE ='"+instance.getTaxCode()+"',";
		sqlEdit = sqlEdit + " REPRESENTATIVE ='"+instance.getRepresentative()+"',";
		sqlEdit = sqlEdit + " POSSITION ='"+instance.getPossition()+"',";
		sqlEdit = sqlEdit + " COMPANY_FAX ='"+instance.getFax()+"',";
		sqlEdit = sqlEdit + " BANK_ACOUNT ='"+instance.getBankAcount()+"',";
		sqlEdit = sqlEdit + " BANK_LOCATION ='"+instance.getBankLocation()+"',";
		sqlEdit = sqlEdit + " COMPANY_WEBSITE ='"+instance.getCompanyWebsite()+"',";
		sqlEdit = sqlEdit + " IMG ='"+instance.getImg()+"',";
		sqlEdit = sqlEdit + " DISPLAY_LOGO ='"+instance.getDisplayLogo()+"';";
		return sqlEdit;
	}

	public void setSqlEdit(String sqlEdit) {
		this.sqlEdit = sqlEdit;
	}
	
	
}
