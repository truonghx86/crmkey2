package altalab.altacrm.action;

import altalab.altacrm.consts.CommonValueConsts;
import altalab.altacrm.model.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.faces.context.FacesContext;

@Name("customersList")
public class CustomersList extends EntityQuery<Customers> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7627576266514742068L;

	private static final String EJBQL = "select customers from Customers customers";
	private Integer userIdLogin = (Integer)FacesContext.getCurrentInstance()
	.getExternalContext().getSessionMap().get(CommonValueConsts.CURRENT_USER_ID_SESSION_NAME);
	private static final String[] RESTRICTIONS = {
			"lower(customers.customerAddress) like lower(concat('%',#{customersList.customers.customerAddress},'%'))",
			"lower(customers.customerCode) like lower(concat('%',#{customersList.customers.customerCode},'%'))",
			"lower(customers.customerDescription) like lower(concat('%',#{customersList.customers.customerDescription},'%'))",
			"lower(customers.customerName) like lower(concat('%',#{customersList.customers.customerName},'%'))",
			"lower(customers.fax) like lower(concat('%',#{customersList.customers.fax},'%'))",
			"lower(customers.taxCode) like lower(concat('%',#{customersList.customers.taxCode},'%'))",
			"lower(customers.telephone) like lower(concat('%',#{customersList.customers.telephone},'%'))",};

	private Customers customers = new Customers();
	public Date startDateDeployment;
	public Date toDateDeployment;
	public Date startDateCreate;
	public Date toDateCreate;
	public Date startDateFinish;
	public Date toDateFinish;
	
	
	public Date getStartDateDeployment() {
		return startDateDeployment;
	}

	/**
	 * @param startDateDeployment the startDateDeployment to set
	 */
	public void setStartDateDeployment(Date startDateDeployment) {
		this.startDateDeployment = startDateDeployment;
	}

	/**
	 * @return the toDateDeployment
	 */
	public Date getToDateDeployment() {
		return toDateDeployment;
	}

	/**
	 * @param toDateDeployment the toDateDeployment to set
	 */
	public void setToDateDeployment(Date toDateDeployment) {
		this.toDateDeployment = toDateDeployment;
	}

	/**
	 * @return the startDateCreate
	 */
	public Date getStartDateCreate() {
		return startDateCreate;
	}

	/**
	 * @param startDateCreate the startDateCreate to set
	 */
	public void setStartDateCreate(Date startDateCreate) {
		this.startDateCreate = startDateCreate;
	}

	/**
	 * @return the toDateCreate
	 */
	public Date getToDateCreate() {
		return toDateCreate;
	}

	/**
	 * @param toDateCreate the toDateCreate to set
	 */
	public void setToDateCreate(Date toDateCreate) {
		this.toDateCreate = toDateCreate;
	}

	public Date getStartDateFinish() {
		return startDateFinish;
	}

	public void setStartDateFinish(Date startDateFinish) {
		this.startDateFinish = startDateFinish;
	}

	public Date getToDateFinish() {
		return toDateFinish;
	}

	public void setToDateFinish(Date toDateFinish) {
		this.toDateFinish = toDateFinish;
	}

	public CustomersList() {
		setEjbql(EJBQL);
		String condition = "((customers.regUser = "
			+ userIdLogin
			+ ") or "
			+ "('"+userIdLogin+"'=#{'1'}))";
		List<String> res = new LinkedList<String>(Arrays.asList(RESTRICTIONS));
		res.add(condition);
		setRestrictionExpressionStrings(res);
		setOrder("customerId desc");
		setMaxResults(25);
	}
	private List<String> RESTRICTIONS_LIST = new ArrayList<String>();
	public void searchCustomers(){
		String condition = "((customers.regUser = "
			+ userIdLogin
			+ ") or "
			+ "('"+userIdLogin+"'=#{'1'}))";
		RESTRICTIONS_LIST.add(condition);
		if(customers.getCustomerAddress() != null && !customers.getCustomerAddress().trim().equals("")){
			RESTRICTIONS_LIST.add("lower(customers.customerAddress) like lower(concat('%',#{customersList.customers.customerAddress},'%'))");				
		}
		if(customers.getCustomerCode() != null && !customers.getCustomerCode().trim().equals("")){
			RESTRICTIONS_LIST.add("lower(customers.customerCode) like lower(concat('%',#{customersList.customers.customerCode},'%'))");				
		}
		if(customers.getCustomerAddress() != null && !customers.getCustomerAddress().trim().equals("")){
			RESTRICTIONS_LIST.add("lower(customers.customerAddress) like lower(concat('%',#{customersList.customers.customerAddress},'%'))");				
		}
		if(customers.getCustomerDescription() != null && !customers.getCustomerDescription().trim().equals("")){
			RESTRICTIONS_LIST.add("lower(customers.customerDescription) like lower(concat('%',#{customersList.customers.customerDescription},'%'))");				
		}
		if(customers.getCustomerName() != null && !customers.getCustomerName().trim().equals("")){
			RESTRICTIONS_LIST.add("lower(customers.customerName) like lower(concat('%',#{customersList.customers.customerName},'%'))");				
		}
		if(customers.getFax() != null && !customers.getFax().trim().equals("")){
			RESTRICTIONS_LIST.add("lower(customers.fax) like lower(concat('%',#{customersList.customers.fax},'%'))");				
		}
		if(customers.getTaxCode() != null && !customers.getTaxCode().trim().equals("")){
			RESTRICTIONS_LIST.add("lower(customers.taxCode) like lower(concat('%',#{customersList.customers.taxCode},'%'))");				
		}
		if(startDateDeployment != null){
			RESTRICTIONS_LIST.add("(customers.deploymentDate >= #{customersList.getStartDateDeployment()})");
		}
		if(toDateDeployment != null){
			RESTRICTIONS_LIST.add("customers.deploymentDate <= #{customersList.getToDateDeployment()}");			
		}
		if(startDateCreate != null){
			RESTRICTIONS_LIST.add("(customers.regDttm >= #{customersList.getStartDateCreate()})");
		}
		if(toDateCreate != null){
			RESTRICTIONS_LIST.add("customers.regDttm <= #{customersList.getToDateCreate()}");			
		}
		if(startDateFinish != null){
			RESTRICTIONS_LIST.add("(customers.finishDate >= #{customersList.getStartDateFinish()})");
		}
		if(toDateFinish != null){
			RESTRICTIONS_LIST.add("customers.finishDate <= #{customersList.getToDateFinish()}");			
		}
		setRestrictionExpressionStrings(RESTRICTIONS_LIST);
		setOrder("customerId desc");
		RESTRICTIONS_LIST = new ArrayList<String>();
	}
	public Customers getCustomers() {
		return customers;
	}
}
