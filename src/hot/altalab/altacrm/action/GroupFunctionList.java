package altalab.altacrm.action;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;

import altalab.altacrm.model.GroupFunctions;
import java.util.Arrays;

@Name("groupFunctionList")
public class GroupFunctionList extends EntityQuery<GroupFunctions> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String EJBQL = "select groupFunctions from GroupFunctions groupFunctions";

	private static final String[] RESTRICTIONS = {};

	private GroupFunctions groupFunction = new GroupFunctions();

	public GroupFunctionList() {
		setEjbql(EJBQL);
		setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
		setMaxResults(25);
	}

	public GroupFunctions getGroupFunction() {
		return groupFunction;
	}

}
