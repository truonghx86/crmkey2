package altalab.altacrm.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.framework.EntityQuery;
import altalab.altacrm.consts.CommonValueConsts;
import altalab.altacrm.model.TblUsers;
import java.util.Arrays;
import javax.faces.context.FacesContext;

@Name("tblProfilesList")
@Scope(ScopeType.CONVERSATION)
public class TblProfilesList extends EntityQuery<TblUsers> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	private static final String EJBQL = "select tblUsers from TblUsers tblUsers";
	private static final String[] RESTRICTIONS = {
			"lower(tblUsers.userAddress) like lower(concat(#{tblUsersList.tblUsers.userAddress},'%'))",
			"lower(tblUsers.userEmail) like lower(concat(#{tblUsersList.tblUsers.userEmail},'%'))",
			"lower(tblUsers.userFullName) like lower(concat(#{tblUsersList.tblUsers.userFullName},'%'))",
			"lower(tblUsers.userMobile) like lower(concat(#{tblUsersList.tblUsers.userMobile},'%'))",
			"lower(tblUsers.password) like lower(concat(#{tblUsersList.tblUsers.password},'%'))",
			"lower(tblUsers.personalId) like lower(concat(#{tblUsersList.tblUsers.personalId},'%'))",
			"lower(tblUsers.userTelephone) like lower(concat(#{tblUsersList.tblUsers.userTelephone},'%'))",
			"lower(tblUsers.userName) like lower(concat(#{tblUsersList.tblUsers.userName},'%'))",};

	private TblUsers tblUsers = new TblUsers();

	public TblProfilesList() {
		setEjbql(EJBQL);
		setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
		setMaxResults(25);
	}

	public TblUsers getTblUsers() {		
		TblUsers sesssionUser = (TblUsers)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(CommonValueConsts.CURRENT_USER_SESSION_NAME);
		tblUsers = getEntityManager().find(TblUsers.class, sesssionUser.getUserId());
		return tblUsers;
	}

}
