package altalab.altacrm.action;

import java.util.ArrayList;
import java.util.List;
import altalab.altacrm.model.FunctionsList;
import altalab.altacrm.model.FunctionsListName;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.framework.EntityHome;

@SuppressWarnings("serial")
@Name("functionHome")
@AutoCreate
@Scope(ScopeType.CONVERSATION)
public class FunctionHome extends EntityHome<FunctionsListName> {

	public void setFunctionsListNameId(Integer id) {
		setId(id);
	}

	public Integer getFunctionsListNameId() {
		return (Integer) getId();
	}

	@Override
	protected FunctionsListName createInstance() {
		FunctionsListName functionsListName = new FunctionsListName();
		return functionsListName;
	}

	public void load() {
		if (isIdDefined()) {
			wire();
		}
	}

	public void wire() {
		getInstance();
	}

	public boolean isWired() {
		return true;
	}

	public FunctionsListName getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}

	@SuppressWarnings("unchecked")
	public void addListFunctions(){
		if(allListFunctions == null || allListFunctions.size() == 0){
			allListFunctions = getEntityManager().createQuery("from FunctionsList group by functionGroup").getResultList();
		}
	}
	public List<FunctionsList> getAllListFunctions() {		
		return allListFunctions;
	}

	private List<FunctionsList> allListFunctions = new ArrayList<FunctionsList>();

	public void addAllFunction() {	
		for(FunctionsList functionsList : allListFunctions){
			functionsList.setSelected(onSelected);
		}
	}
	private Boolean onSelected;

	public Boolean getOnSelected() {
		return onSelected;
	}

	public void setOnSelected(Boolean onSelected) {
		this.onSelected = onSelected;
	}
	@SuppressWarnings("unchecked")
	public String addFunctionListName(){
		getEntityManager().persist(instance);
		getEntityManager().flush();
		String sql = "insert into functions_list_middle (FUNCTIONS_LIST_NAME_ID, FUNCTION_ID) values ";
		for(FunctionsList functionsList : allListFunctions){
			if(functionsList.getSelected()){
				List<FunctionsList> listFunctionInsert = getEntityManager().createQuery(
						"from FunctionsList where functionGroup = '"+functionsList.getFunctionGroup()+"'")
						.getResultList();
				for(FunctionsList f : listFunctionInsert){
					sql = sql + "("+instance.getFunctionsListNameId()+","+f.getFunctionId()+"),";
				}	
			}
		}
		sql = sql.substring(0, sql.length() - 1) + ";";
		getEntityManager().createNativeQuery(sql).executeUpdate();
		getEntityManager().flush();
		createdMessage();
		return "persisted";
	}

	@SuppressWarnings("unchecked")
	public void preEdit(){
		addListFunctions();
		String sql = "select FUNCTION_GROUP from Function_List fl join Functions_List_Middle m on fl.function_Id = m.FUNCTION_ID " +
		"join functions_list_name n on n.FUNCTIONS_LIST_NAME_ID = m.FUNCTIONS_LIST_NAME_ID where n.FUNCTIONS_LIST_NAME_ID = " 
		+getFunctionsListNameId()+" group by function_Group ";
		List<Object> listGroupFunction = getEntityManager().createNativeQuery(sql).getResultList();

		for(FunctionsList functionsList : allListFunctions){
			if(listGroupFunction.contains(functionsList.getFunctionGroup())){
				functionsList.setSelected(true);
			}
		}

	}
	@SuppressWarnings("unchecked")
	public String updateFunctionListName(){
		getEntityManager().createNativeQuery
		("delete from functions_list_middle where FUNCTIONS_LIST_NAME_ID = "+instance.getFunctionsListNameId()).executeUpdate();
		String sql = "insert into functions_list_middle (FUNCTIONS_LIST_NAME_ID, FUNCTION_ID) values ";

		for(FunctionsList functionsList : allListFunctions){
			if(functionsList.getSelected()){
				List<FunctionsList> listFunctionInsert = getEntityManager().createQuery(
						"from FunctionsList where functionGroup = '"+functionsList.getFunctionGroup()+"'")
						.getResultList();
				for(FunctionsList f : listFunctionInsert){
					sql = sql + "("+instance.getFunctionsListNameId()+","+f.getFunctionId()+"),";
				}	
			}
		}
		sql = sql.substring(0, sql.length() - 1) + ";";
		getEntityManager().createNativeQuery(sql).executeUpdate();
		getEntityManager().merge(instance);
		getEntityManager().flush();
		updatedMessage();
		return "updated";		
	}
	public String removeFunctionListName(){
		getEntityManager().createNativeQuery
		("delete from functions_list_middle where FUNCTIONS_LIST_NAME_ID = "+instance.getFunctionsListNameId()).executeUpdate();
		return super.remove();
	}
	private String sqlAdd;

	public String getSqlAdd() {
		return sqlAdd;
	}

	@SuppressWarnings("unchecked")
	public void setSqlAdd(Integer functionsListNameId) {
		List<FunctionsList> listFunctionInsert = getEntityManager().createQuery(
				"select functionsList from FunctionsList as functionsList inner  join functionsList.functionsListMiddle as functionsListMiddle" 
				+" where functionsListMiddle.id.functionsListNameId = "+functionsListNameId)
				.getResultList();
		String sqlTotal = "";
		
		for(FunctionsList f : listFunctionInsert){
			String sql = "insert into tbl_functions (`FUNCTION_ID`,`DESCRIPTION`,`FUNCTION_NAME`,`FUNCTION_CODE`,`FUNCTION_GROUP`)  values ";
			sql = sql + "("+f.getFunctionId()+",'"+f.getDescription()+"','"+f.getFunctionName()+"','"+f.getFunctionCode()+"','"+f.getFunctionGroup()+"');\n";
			sqlTotal = sqlTotal + sql;
		}
//		sql = sql.substring(0, sql.length() - 1) + ";";
		this.sqlAdd = sqlTotal;
		String sqlListFunction = "select FUNCTION_GROUP from Function_List fl join Functions_List_Middle m on fl.function_Id = m.FUNCTION_ID " +
		"join functions_list_name n on n.FUNCTIONS_LIST_NAME_ID = m.FUNCTIONS_LIST_NAME_ID where n.FUNCTIONS_LIST_NAME_ID = " +functionsListNameId+
		" group by function_Group ";
		listFunctionsDetail = (List<String>)getEntityManager().createNativeQuery(sqlListFunction).getResultList();
		
		
	}
	private List<String> listFunctionsDetail = new ArrayList<String>();

	public List<String> getListFunctionsDetail() {
		return listFunctionsDetail;
	}

	public void setListFunctionsDetail(List<String> listFunctionsDetail) {
		this.listFunctionsDetail = listFunctionsDetail;
	}
	
}
