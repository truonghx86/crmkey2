package altalab.altacrm.action;

import java.util.ArrayList;
import java.util.List;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.framework.EntityHome;

import altalab.altacrm.consts.CommonValueConsts;
import altalab.altacrm.model.Customers;
import altalab.altacrm.model.GroupFunctionUser;
import altalab.altacrm.model.GroupFunctionUserId;
import altalab.altacrm.model.GroupFunctions;
import altalab.altacrm.model.TblUsers;
import altalab.altacrm.utils.MD5Utils;

@Name("tblUsersHome")
@AutoCreate
@Scope(ScopeType.CONVERSATION)
public class TblUsersHome extends EntityHome<TblUsers> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void setTblUsersUserId(Integer id) {
		setId(id);
	}

	public Integer getTblUsersUserId() {
		return (Integer) getId();
	}

	@Override
	protected TblUsers createInstance() {
		TblUsers tblUsers = new TblUsers();
		return tblUsers;
	}

	public void load() {
		if (isIdDefined()) {
			wire();
		}
	}

	public void wire() {
		getInstance();
	}

	public boolean isWired() {
		return true;
	}

	public TblUsers getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}
	
	@SuppressWarnings("unchecked")
	public List<TblUsers> search(String sql) {
		return getEntityManager().createQuery(sql).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public String addUser() {		
		// kiểm tra trùng user
		List<TblUsers> checkUser = getEntityManager().createQuery(
				"from TblUsers where userName = " + "'"
						+ instance.getUserName() + "'").getResultList();
		if (checkUser.size() > 0) {
			addFacesMessageFromResourceBundle("userName_already_exists", "");
			return "";
		}
		try {
			instance.setPassword(MD5Utils.getMD5(instance.getPassword()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.persist();
		
		return "persisted";
	}
	
	@SuppressWarnings("unchecked")
	public String updateUser() {		
		// check trung user
		List<TblUsers> checkUser = getEntityManager().createQuery(
				"from TblUsers where userName = " + "'"
						+ instance.getUserName() + "' and userId != "
						+ instance.getUserId()).getResultList();
		if (checkUser.size() > 0) {
			addFacesMessageFromResourceBundle("userName_already_exists", "");
			return "";
		}
		
		return this.update();
	}
	
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String resetPassword() {
		getInstance();
		if (newPassword == null || newPassword.trim().equals("")) {
			addFacesMessageFromResourceBundle("users.password.not.null", "");
			return "";
		}
		instance.setPassword(MD5Utils.getMD5(newPassword));
		this.update();
		return "updated";
	}
	
	private String oldPassword, newPassword, newPasswordCf;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPasswordCf() {
		return newPasswordCf;
	}

	public void setNewPasswordCf(String newPasswordCf) {
		this.newPasswordCf = newPasswordCf;
	}
	public String changePassword() {
		getInstance();
		boolean error = false;
		if (oldPassword == null || !MD5Utils.getMD5(oldPassword).equals(instance.getPassword())) {
			addFacesMessageFromResourceBundle("users.old.password.not.match", "");
			error = true;
		}		
		if (newPassword == null || newPassword.trim().equals("")) {
			addFacesMessageFromResourceBundle("users.password.not.null", "");
			error = true;
		} 
		
		if (error) {
			return "";
		}
		//success	
		instance.setPassword(MD5Utils.getMD5(newPassword));
		this.update();	
		return "updated";	
	}
	
	public String inActive() {		
		instance.setStatus(CommonValueConsts.STATUS_INACTIVE);
		this.update();
		return "inactive";
	}

	public String active() throws NumberFormatException, Exception {		
		instance.setStatus(CommonValueConsts.STATUS_ACTIVE);
		this.update();
		return "activated";
	}
	@SuppressWarnings("unchecked")
	public String removeUser() {		
		List<Customers> customers = getEntityManager().createQuery(
				"from Customers where regUser = "
						+ instance.getUserId()).getResultList();
		if (customers.size() > 0) {
			addFacesMessageFromResourceBundle("customer.exists", "");
			return "";
		}
		List<GroupFunctionUser> listGroupFunctionUser = getEntityManager()
		.createQuery(
				"from GroupFunctionUser where tblUsers.userId = "
						+ instance.getUserId()).getResultList();
		if (listGroupFunctionUser.size() > 0) {
			for (GroupFunctionUser groupFunction : listGroupFunctionUser) {
				getEntityManager().remove(groupFunction);
				getEntityManager().flush();
			}
		}
		this.remove();		
		return "removed";
	}
	private List<GroupFunctions> listGroupFunction = new ArrayList<GroupFunctions>();

	public void addGroupFunction(GroupFunctions groupFunctions) {
		// listFunction.clear();
		if (groupFunctions.getSelected()) {
			listGroupFunction.add(groupFunctions);
		} else {
			listGroupFunction.remove(groupFunctions);
		}
	}

	@SuppressWarnings("unchecked")
	public List<GroupFunctions> getAllListGroupFunctions() {
		return getEntityManager().createQuery("from GroupFunctions")
				.getResultList();
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public String updateGroupFunction() {
		joinTransaction();
		getEntityManager().createNativeQuery(
				"DELETE FROM group_function_user where USER_ID = "
						+ instance.getUserId()).executeUpdate();
		getEntityManager().clear();
		for (GroupFunctions function : listGroupFunction) {
			List<GroupFunctionUser> listGroupFunc = getEntityManager()
					.createQuery(
							"FROM GroupFunctionUser where id.groupFunctionId = "
									+ function.getGroupFunctionId()
									+ " and id.userId = "
									+ instance.getUserId()).getResultList();
			if (listGroupFunc.size() == 0) {
				GroupFunctionUser groupFunctionUser = new GroupFunctionUser();
				GroupFunctionUserId groupFunctionUserId = new GroupFunctionUserId();
				groupFunctionUserId.setGroupFunctionId(function
						.getGroupFunctionId());
				groupFunctionUserId.setUserId(instance.getUserId());
				groupFunctionUser.setId(groupFunctionUserId);
				groupFunctionUser.setTblUsers(instance);
				groupFunctionUser.setGroupFunction(function);
				getEntityManager().persist(groupFunctionUser);
				getEntityManager().flush();
			}
		}
		return "updated";
	}
	@SuppressWarnings("unchecked")
	public void addListGroupFunctions() {
		getInstance();
		listGroupFunction = new ArrayList<GroupFunctions>();
		List<GroupFunctionUser> listGroup = getEntityManager().createQuery(
				"From GroupFunctionUser where tblUsers.userId = "
						+ instance.getUserId()).getResultList();
		for (GroupFunctionUser group : listGroup) {
			GroupFunctions groupFunction = group.getGroupFunction();
			groupFunction.setSelected(true);
			listGroupFunction.add(groupFunction);
		}
	}
}
