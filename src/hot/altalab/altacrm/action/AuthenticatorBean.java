package altalab.altacrm.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpSession;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.Identity;

import altalab.altacrm.consts.CommonValueConsts;
import altalab.altacrm.model.GroupFunctionUser;
import altalab.altacrm.model.TblFunctions;
import altalab.altacrm.model.TblGroupFunctions;
import altalab.altacrm.model.TblUsers;
import altalab.altacrm.utils.MD5Utils;

@Stateless
@Name("authenticator")
public class AuthenticatorBean implements Authenticator {
	@Logger
	private Log log;

	@In
	Identity identity;
	@In
	Credentials credentials;
	@In
	EntityManager entityManager;
	@In @Out
	TblUsers tblUsers;
	
	public static Map<Long, String> userTrackerMap = new HashMap<Long, String>();

	@SuppressWarnings("unchecked")
	public boolean authenticate() {
		log.info("authenticating {0}", credentials.getUsername());
		
		List<TblUsers> userList = (List<TblUsers>) entityManager.createQuery(
				"from TblUsers where status = "
						+ CommonValueConsts.STATUS_ACTIVE + " and userName='"
						+ credentials.getUsername() + "'").getResultList();
		if(null == userList || userList.size() == 0)
			return false;

		tblUsers = userList.get(0);
		if (!tblUsers.getPassword().equals(MD5Utils.getMD5(credentials.getPassword())))
			return false;

		//set id to session
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(CommonValueConsts.CURRENT_USER_ID_SESSION_NAME, tblUsers.getUserId());
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(CommonValueConsts.CURRENT_USER_SESSION_NAME, tblUsers);

		//remove current user and put this user to tracker
		userTrackerMap.remove(tblUsers.getUserId());
		userTrackerMap.put(tblUsers.getUserId().longValue(), getSession().getId());
		if (tblUsers.getUserName().equals("admin")) {
			List<TblFunctions> listFunction = (List<TblFunctions>) entityManager
					.createQuery("from TblFunctions").getResultList();
			for (TblFunctions fun : listFunction) {
				identity.addRole(fun.getFunctionCode());
			}

		} else {
			List<GroupFunctionUser> listGroupFunctionUser = (List<GroupFunctionUser>) entityManager
					.createQuery(
							"from GroupFunctionUser where tblUsers.userId = "
									+ tblUsers.getUserId()).getResultList();
			for (GroupFunctionUser groupFunctionUser : listGroupFunctionUser) {
				List<TblGroupFunctions> listGroupFunction = (List<TblGroupFunctions>) entityManager
						.createQuery(
								"from TblGroupFunctions where groupFunctions.groupFunctionId = "
										+ groupFunctionUser.getId()
												.getGroupFunctionId())
						.getResultList();
				if (listGroupFunction.size() > 0) {
					for (TblGroupFunctions groupFunction : listGroupFunction) {
						identity.addRole(groupFunction.getTblFunctions()
								.getFunctionCode());
					}
				}

			}
		}
		return true;		
	}
	
	public static HttpSession getSession(){
		return (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	}

}
