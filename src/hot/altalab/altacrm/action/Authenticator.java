package altalab.altacrm.action;

import javax.ejb.Local;

@Local
public interface Authenticator {

	boolean authenticate();

}
