package altalab.altacrm.action;
import java.util.List;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.framework.EntityHome;
import altalab.altacrm.model.TblUsers;
import altalab.altacrm.utils.MD5Utils;

@Name("tblProfilesHome")
@AutoCreate
@Scope(ScopeType.CONVERSATION)
public class TblProfilesHome extends EntityHome<TblUsers> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void setTblUsersUserId(Integer id) {
		setId(id);
	}

	public Integer getTblUsersUserId() {
		return (Integer) getId();
	}

	@Override
	protected TblUsers createInstance() {
		TblUsers tblUsers = new TblUsers();
		return tblUsers;
	}

	public void load() {
		if (isIdDefined()) {
			wire();
		}
	}

	public void wire() {
		getInstance();
	}

	public boolean isWired() {
		return true;
	}

	public TblUsers getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}

	@SuppressWarnings("unchecked")
	public List<TblUsers> search(String sql) {
		return getEntityManager().createQuery(sql).getResultList();
	}
	private Integer userGroupId;

	public Integer getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Integer userGroupId) {
		this.userGroupId = userGroupId;
	}


	private String oldPassword, newPassword, newPasswordCf;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPasswordCf() {
		return newPasswordCf;
	}

	public void setNewPasswordCf(String newPasswordCf) {
		this.newPasswordCf = newPasswordCf;
	}
	public String changePassword() {
		getInstance();
		boolean error = false;
		if (oldPassword == null || !MD5Utils.getMD5(oldPassword).equals(instance.getPassword())) {
			addFacesMessageFromResourceBundle("users.old.password.not.match", "");
			error = true;
		}		
		if (newPassword == null || newPassword.trim().equals("")) {
			addFacesMessageFromResourceBundle("users.password.not.null", "");
			error = true;
		} 
		
		if (error) {
			return "";
		}
		//success	
		instance.setPassword(MD5Utils.getMD5(newPassword));
		this.update();	
		return "updated";	
	}
	public TblUsers getUserById(Integer id) {
		return getEntityManager().find(TblUsers.class, id);
	}
}
