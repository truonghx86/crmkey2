package altalab.altacrm.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.framework.EntityQuery;

import altalab.altacrm.model.TblUsers;

import java.util.Arrays;

@Name("tblUsersList")
@Scope(ScopeType.CONVERSATION)
public class TblUsersList extends EntityQuery<TblUsers> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3059771452882544611L;

	private static final String EJBQL = "select tblUsers from TblUsers tblUsers";

	private static final String[] RESTRICTIONS = {
			"lower(tblUsers.fullName) like lower(concat('%',#{tblUsersList.tblUsers.fullName},'%'))",			
			"lower(tblUsers.telephone) like lower(concat('%',#{tblUsersList.tblUsers.telephone},'%'))",
			"lower(tblUsers.userName) like lower(concat('%',#{tblUsersList.tblUsers.userName},'%'))",};

	private TblUsers tblUsers = new TblUsers();

	public TblUsersList() {
		setEjbql(EJBQL);
		setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
		setMaxResults(25);
	}

	public TblUsers getTblUsers() {
		return tblUsers;
	}

}
