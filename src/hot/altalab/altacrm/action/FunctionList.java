package altalab.altacrm.action;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;

import altalab.altacrm.model.FunctionsListName;
import java.util.Arrays;

@Name("functionList")
public class FunctionList extends EntityQuery<FunctionsListName> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String EJBQL = "select functionsListName from FunctionsListName functionsListName";

	private static final String[] RESTRICTIONS = {};

	private FunctionsListName functionsListName = new FunctionsListName();

	public FunctionList() {
		setEjbql(EJBQL);
		setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
		setMaxResults(25);
	}

	public FunctionsListName getFunctionsListName() {
		return functionsListName;
	}

}
