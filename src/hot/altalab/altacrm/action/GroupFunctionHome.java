package altalab.altacrm.action;

import java.util.ArrayList;
import java.util.List;
import altalab.altacrm.model.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.framework.EntityHome;
import org.richfaces.component.UITree;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;

@SuppressWarnings("serial")
@Name("groupFunctionHome")
@AutoCreate
@Scope(ScopeType.CONVERSATION)
public class GroupFunctionHome extends EntityHome<GroupFunctions> {

	public void setGroupFunctionId(Integer id) {
		setId(id);
	}

	public Integer getGroupFunctionId() {
		return (Integer) getId();
	}

	@Override
	protected GroupFunctions createInstance() {
		GroupFunctions groupFunctions = new GroupFunctions();
		return groupFunctions;
	}

	public void load() {
		if (isIdDefined()) {
			wire();
		}
	}

	public void wire() {
		getInstance();
	}

	public boolean isWired() {
		return true;
	}

	public GroupFunctions getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}

	@SuppressWarnings("unchecked")
	public List<TblFunctions> getAllListFunctions() {
		return getEntityManager().createQuery("from TblFunctions")
		.getResultList();
	}

	private List<TblFunctions> listFunction = new ArrayList<TblFunctions>();

	public void addFunction(TblFunctions functions) {
		// listFunction.clear();
		if (functions.getSelected()) {
			listFunction.add(functions);
		} else {
			listFunction.remove(functions);			
		}
	}

	@SuppressWarnings("unchecked")
	public String persistGroupFunction() {
		List<GroupFunctions> listGroupFunction = getEntityManager().createQuery(
				"from GroupFunctions where groupFunctionName = '" + instance.getGroupFunctionName()+ "'").getResultList();
		if(listGroupFunction.size() > 0){
			addFacesMessageFromResourceBundle("groupFunctionName.is.exist", "");
			return "";
		}
		if(listNodeSelected.size() == 0){
			addFacesMessageFromResourceBundle("no_function_select", "");
			return "";
		}
		this.persist();
		String sql = "INSERT INTO Tbl_Group_Functions (GROUP_FUNCTION_ID, FUNCTION_ID) " +
		"VALUES ";
		for(RichTreeNode richTreeNode : listNodeSelected){
			if(richTreeNode.getSelected() != null && richTreeNode.getSelected()){
				sql = sql + "("+instance.getGroupFunctionId()+","+richTreeNode.getValue()+"),";
			}
		}
		sql = sql.substring(0, sql.length()-1);
		getEntityManager().createNativeQuery(sql).executeUpdate();
		getEntityManager().flush();
		return "persisted";
	}

	@SuppressWarnings("unchecked")
	public String updateGroupFunction() {
		List<GroupFunctions> listGroupFunction = getEntityManager().createQuery(
				"from GroupFunctions where groupFunctionName = '" 
				+ instance.getGroupFunctionName() + "' and groupFunctionId != " + instance.getGroupFunctionId()).getResultList();
		if(listGroupFunction.size() > 0){
			addFacesMessageFromResourceBundle("groupFunctionName.is.exist", "");
			return "";
		}
		if(listNodeSelected.size() == 0){
			addFacesMessageFromResourceBundle("no_function_select", "");
			return "";
		}
		this.update();
		getEntityManager().createNativeQuery("delete from Tbl_Group_Functions where GROUP_FUNCTION_ID = "
				+ instance.getGroupFunctionId()).executeUpdate();

		String sql = "INSERT INTO Tbl_Group_Functions (GROUP_FUNCTION_ID, FUNCTION_ID) " +
		"VALUES ";
		for(RichTreeNode richTreeNode : listNodeSelected){
			if(richTreeNode.getSelected() != null && richTreeNode.getSelected()){
				sql = sql + "("+instance.getGroupFunctionId()+","+richTreeNode.getValue()+"),";
			}
		}
		sql = sql.substring(0, sql.length()-1);
		getEntityManager().createNativeQuery(sql).executeUpdate();
		getEntityManager().flush();
		return "updated";
	}	
	public String removeGroupFunction() {	
		getEntityManager().createNativeQuery("delete from group_function_user where GROUP_FUNCTION_ID = " + instance.getGroupFunctionId()).executeUpdate();
		getEntityManager().createNativeQuery("delete from tbl_group_functions where GROUP_FUNCTION_ID = " + instance.getGroupFunctionId()).executeUpdate();
		this.remove();
		return "removed";
	}

	@SuppressWarnings("unchecked")
	public void addListFunctions() {
		getInstance();
		listFunction = new ArrayList<TblFunctions>();
		List<TblGroupFunctions> listGroupFunction = getEntityManager()
		.createQuery(
				"From TblGroupFunctions where groupFunctions.groupFunctionId = "
				+ instance.getGroupFunctionId())
				.getResultList();
		for (TblGroupFunctions tblGroupFunctions : listGroupFunction) {
			TblFunctions func = tblGroupFunctions.getTblFunctions();
			func.setSelected(true);
			listFunction.add(func);
		}
		addNodes();
	}

	private Boolean onSelected;

	public Boolean getOnSelected() {
		return onSelected;
	}

	public void setOnSelected(Boolean onSelected) {
		this.onSelected = onSelected;
	}

	@SuppressWarnings("unchecked")
	public void addAllFunction() {	
		listNodeSelected.clear();
		if(onSelected){
			java.util.Iterator<java.util.Map.Entry<java.lang.Object,TreeNode<RichTreeNode>>> child = rootNode.getChildren();
			int i = 0;
			while(child.hasNext()) {
				i++;
				RichTreeNode element = (RichTreeNode)child.next().getValue().getData();
				element.setSelected(onSelected);
				for(RichTreeNode richTreeNode : element.getListChild()){
					richTreeNode.setSelected(onSelected);
					listNodeSelected.add(richTreeNode);
				}	         
		      }
		}else{
			java.util.Iterator<java.util.Map.Entry<java.lang.Object,TreeNode<RichTreeNode>>> child = rootNode.getChildren();
			int i = 0;
			while(child.hasNext()) {
				i++;
				RichTreeNode element = (RichTreeNode)child.next().getValue().getData();
				element.setSelected(onSelected);
				for(RichTreeNode richTreeNode : element.getListChild()){
					richTreeNode.setSelected(onSelected);
				}	         
		      }
		}
	}
	@SuppressWarnings("unchecked")
	private TreeNode rootNode = null;
	@SuppressWarnings("unchecked")
	public TreeNode getRootNode() {
		return rootNode;
	}

	@SuppressWarnings("unchecked")
	public void setRootNode(TreeNode rootNode) {
		this.rootNode = rootNode;
	}

	@SuppressWarnings("unchecked")
	public void addNodes() {
		int counter = 1;
		int lastCounter = 1;
		rootNode = new TreeNodeImpl();
		List<TblFunctions> listGroupFunctions = getEntityManager().createQuery("from TblFunctions order by functionGroup")
		.getResultList();
		List<Integer> listGroupFunction = new ArrayList<Integer>();
		if(instance != null && instance.getGroupFunctionId() != null){
			listGroupFunction = getEntityManager().createQuery("select tblFunctions.functionId From TblGroupFunctions " +
					"where groupFunctions.groupFunctionId = "
					+ instance.getGroupFunctionId())
					.getResultList();
		}
		
		TblFunctions preFunction = new TblFunctions();
		RichTreeNode data = new RichTreeNode();
		List<RichTreeNode> listChild = new ArrayList<RichTreeNode>();
		TreeNodeImpl<RichTreeNode> nodeImpl = new TreeNodeImpl<RichTreeNode>();	
		for (TblFunctions groupFunctions : listGroupFunctions) {
			if(preFunction.getFunctionName() == null || !preFunction.getFunctionGroup().equals(groupFunctions.getFunctionGroup())){
				if(preFunction.getFunctionName() != null){
					int i = 0;
					for(RichTreeNode richTreeNode : data.getListChild()){
						if(richTreeNode.getSelected() != null && richTreeNode.getSelected()){
							i++;
						}else{
							break;
						}
					}
					if(i == data.getListChild().size()){
						data.setSelected(true);
					}
				}
				data = new RichTreeNode();
				listChild = new ArrayList<RichTreeNode>();
				nodeImpl = new TreeNodeImpl<RichTreeNode>();				
				data.setName(groupFunctions.getFunctionGroup());
				TreeNodeImpl<RichTreeNode> nodeImplChild = new TreeNodeImpl<RichTreeNode>();
				RichTreeNode dataChild = new RichTreeNode();
				dataChild.setName(groupFunctions.getFunctionName());
				dataChild.setValue(groupFunctions.getFunctionId());
				dataChild.setRoot(data);
				nodeImplChild.setData(dataChild);
				listChild.add(dataChild);
				nodeImpl.addChild(new Integer(counter), nodeImplChild);
				counter++;
				data.setListChild(listChild);
				nodeImpl.setData(data);
				rootNode.addChild(new Integer(counter), nodeImpl);	
				if(listGroupFunction.contains(groupFunctions.getFunctionId())){
					dataChild.setSelected(true);
					listNodeSelected.add(dataChild);
				}
				preFunction = groupFunctions;
				lastCounter = counter;

			} else {
				TreeNodeImpl<RichTreeNode> nodeImplChild = new TreeNodeImpl<RichTreeNode>();
				RichTreeNode dataChild = new RichTreeNode();
				dataChild.setName(groupFunctions.getFunctionName());
				dataChild.setValue(groupFunctions.getFunctionId());
				dataChild.setRoot(data);
				nodeImplChild.setData(dataChild);
				listChild.add(dataChild);
				data.setListChild(listChild);
				nodeImpl.addChild(new Integer(counter), nodeImplChild);
				counter++;
				if(listGroupFunction.contains(groupFunctions.getFunctionId())){
					dataChild.setSelected(true);
					listNodeSelected.add(dataChild);
				}
			}			
		}
		int j = 0;
		for(RichTreeNode richTreeNode : data.getListChild()){				
			if(richTreeNode.getSelected() != null && richTreeNode.getSelected()){
				j++;
			}else{
				break;
			}
		}
		if(j == data.getListChild().size()){
			data.setSelected(true);
			nodeImpl.setData(data);
			rootNode.addChild(lastCounter, nodeImpl);
		}

	}
	public Boolean adviseNodeOpened(UITree tree){
		return Boolean.TRUE;
	}
	private List<RichTreeNode> listNodeSelected = new ArrayList<RichTreeNode>();

	public List<RichTreeNode> getListNodeSelected() {
		return listNodeSelected;
	}

	public void setListNodeSelected(List<RichTreeNode> listNodeSelected) {
		this.listNodeSelected = listNodeSelected;
	}
	public void checkNode(RichTreeNode node){
		if(node.getRoot() != null){
			if (node.getSelected()) {
				listNodeSelected.add(node);
				int i = 0;
				for(RichTreeNode nodeChild : node.getRoot().getListChild()){
					if(nodeChild.getSelected() != null && nodeChild.getSelected()){
						i++;
					}else{
						break;
					}
				}
				if(i == node.getRoot().getListChild().size()){
					node.getRoot().setSelected(node.getSelected());
				}
			} else {
				node.getRoot().setSelected(node.getSelected());
				listNodeSelected.remove(node);
			}
		}else{
			if (node.getSelected()) {
				for(RichTreeNode richTreeNode : node.getListChild()){
					richTreeNode.setSelected(node.getSelected());
					if(!listNodeSelected.contains(richTreeNode)){
						listNodeSelected.add(richTreeNode);
					}
				}						
			} else {
				for(RichTreeNode richTreeNode : node.getListChild()){
					richTreeNode.setSelected(node.getSelected());
					listNodeSelected.remove(richTreeNode);	
				}
			}			
		}
	}
}
