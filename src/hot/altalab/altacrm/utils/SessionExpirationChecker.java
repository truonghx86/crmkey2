/**
 * License Agreement.
 *
 *  JBoss RichFaces - Ajax4jsf Component Library
 *
 * Copyright (C) 2007  Exadel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package altalab.altacrm.utils;

import java.util.Map;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.jboss.seam.web.Session;

import altalab.altacrm.action.AuthenticatorBean;
import altalab.altacrm.model.TblUsers;

/**
 * Utility class for check is the user session was expired or user were login in another browser.
 * Observes <code>Constants.CHECK_USER_EXPIRED_EVENT</code> event
 * @author Andrey Markhel
 */
@Scope(ScopeType.EVENT)
@Name("sessionExpirationChecker")
@AutoCreate
public class SessionExpirationChecker {

	@In(required = false) TblUsers tblUsers;
	@In Identity identity;
	
	
	/**
	 * Utility method for check is the user session was expired or user were login in another browser.
	 * Observes <code>Constants.CHECK_USER_EXPIRED_EVENT</code> event.
	 * Redirects to error page if user were login in another browser.
	 * @param session - user's session
	 */
	@Observer("CHECK_USER_EXPIRED_EVENT")
	public void checkUserExpiration(HttpSession session){
		if(isShouldExpireUser(session)){
	    	 try {
	    		Session.instance().invalidate();
//	    		FacesContext fc = FacesContext.getCurrentInstance();
//	    		fc.getApplication().getNavigationHandler().handleNavigation(fc, null, "login.seam");
	    		FacesContext fc = FacesContext.getCurrentInstance();
	    		UIViewRoot viewRoot = 
	    		fc.getApplication().getViewHandler().createView(fc, "/login.xhtml");
	    		fc.setViewRoot(viewRoot);
	    		fc.renderResponse();
			} catch (Exception e1) {
				FacesContext.getCurrentInstance().responseComplete();
			}
	     }
	}
	
	private boolean isShouldExpireUser(HttpSession session) {
		Map<Long, String> userTracker = AuthenticatorBean.userTrackerMap;
		
		if(!identity.isLoggedIn())
			return false;
				
		String currentSessionKey = userTracker.get(tblUsers.getUserId().longValue());
		if(null == currentSessionKey)
			return false;
		
		return ((session != null) && !session.getId().equals(currentSessionKey));
		
	}
	
	
}
