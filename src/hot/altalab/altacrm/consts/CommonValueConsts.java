/**
 * 
 */
package altalab.altacrm.consts;

import org.jboss.seam.annotations.Name;

/**
 * 
 * <p> Class: CommonValueConsts </p>
 * <p> Package: altalab.altacrm.consts </p> 
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 * <p> Version: $1.0 </p>
 * <p> Create date: Jan 10, 2013 9:57:33 AM </p>
 * <p> Update date: Jan 10, 2013 9:57:33 AM </p>
 *
 */
@Name("commonValueConsts")
public class CommonValueConsts {		
	
	public static final String CURRENT_USER_ID_SESSION_NAME = "curentUserIdSessionName";
	public static final String CURRENT_USER_SESSION_NAME = "curentUserSessionName";
	public static final Integer STATUS_ACTIVE = 1;
	public static final Integer STATUS_INACTIVE = 9;

}
