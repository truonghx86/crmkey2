/**
 * <p> File: altalab.altacrm.model.RichTreeNode.java </p>
 * <p> Project: altacrmDemo </p>
 * <p> Copyright altalab. </p>
 * <p> All rights reserved. </p>
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 * <p> Version: $1.0 </p>
 * <p> Create date: Feb 22, 2013 1:57:26 PM </p>
 * <p> Update date: Feb 22, 2013 1:57:26 PM </p>
 **/

package altalab.altacrm.model;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> Class: RichTreeNode </p>
 * <p> Package: altalab.altacrm.model </p> 
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 * <p> Version: $1.0 </p>
 * <p> Create date: Feb 22, 2013 1:57:26 PM </p>
 * <p> Update date: Feb 22, 2013 1:57:26 PM </p>
 **/
public class RichTreeNode implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3944004270220821234L;
	private Integer value;
	private Boolean selected;
	private String name;
	private RichTreeNode root;
	private List<RichTreeNode> listChild = new ArrayList<RichTreeNode>();
	
	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RichTreeNode getRoot() {
		return root;
	}

	public void setRoot(RichTreeNode root) {
		this.root = root;
	}

	public List<RichTreeNode> getListChild() {
		return listChild;
	}

	public void setListChild(List<RichTreeNode> listChild) {
		this.listChild = listChild;
	}
	
}
