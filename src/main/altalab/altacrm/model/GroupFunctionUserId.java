/**
 * <p> File: altalab.altacrm.model.GroupFunctionsId.java </p>
 * <p> Project: altacrmDemo </p>
 * <p> Copyright altalab. </p>
 * <p> All rights reserved. </p>
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 * <p> Version: $1.0 </p>
 * <p> Create date: Feb 28, 2013 9:48:54 AM </p>
 * <p> Update date: Feb 28, 2013 9:48:54 AM </p>
 **/

package altalab.altacrm.model;

import javax.persistence.Column;

/**
 **/
public class GroupFunctionUserId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer groupFunctionId;
	private Integer userId;

	@Column(name = "GROUP_FUNCTION_ID", nullable = false)
	public Integer getGroupFunctionId() {
		return groupFunctionId;
	}

	public void setGroupFunctionId(Integer groupFunctionId) {
		this.groupFunctionId = groupFunctionId;
	}

	@Column(name = "USER_ID", nullable = false)
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
