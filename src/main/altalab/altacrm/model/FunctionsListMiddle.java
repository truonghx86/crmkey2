package altalab.altacrm.model;

// Generated Nov 10, 2010 11:14:48 AM by Hibernate Tools 3.2.4.GA

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.NotNull;

/**
 * 
 * <p>
 * Class: TblGroupFunctions
 * </p>
 * <p>
 * Package: altalab.altacrm.model
 * </p>
 * <p>
 * Author: truonghx
 * </p>
 * <p>
 * Update by: truonghx

 * 
 */
@Entity
@Table(name = "functions_list_middle")
public class FunctionsListMiddle implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private FunctionsListMiddleId id;
	private FunctionsList functionsList;
	private FunctionsListName functionsListNameId;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "functionsListNameId", column = @Column(name = "FUNCTIONS_LIST_NAME_ID", nullable = false)),
			@AttributeOverride(name = "functionId", column = @Column(name = "FUNCTION_ID", nullable = false)) })
	@NotNull
	public FunctionsListMiddleId getId() {
		return id;
	}

	public void setId(FunctionsListMiddleId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FUNCTION_ID", nullable = false, insertable = false, updatable = false)
	@NotNull
	public FunctionsList getFunctionsList() {
		return functionsList;
	}

	public void setFunctionsList(FunctionsList functionsList) {
		this.functionsList = functionsList;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "FUNCTIONS_LIST_NAME_ID", nullable = false, insertable = false, updatable = false)
	@NotNull
	public FunctionsListName getFunctionsListNameId() {
		return functionsListNameId;
	}

	public void setFunctionsListNameId(FunctionsListName functionsListNameId) {
		this.functionsListNameId = functionsListNameId;
	}
}
