package altalab.altacrm.model;

// Generated Nov 10, 2010 11:14:48 AM by Hibernate Tools 3.2.4.GA


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.Length;

@Entity
@Table(name = "functions_list_name")
public class FunctionsListName implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer functionsListNameId;
	private String functionsListName;
	private String description;
	private Boolean selected;
	private List<FunctionsListMiddle> functionsListMiddle = new ArrayList<FunctionsListMiddle>();
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "FUNCTIONS_LIST_NAME_ID", unique = true, nullable = false)
	public Integer getFunctionsListNameId() {
		return functionsListNameId;
	}

	public void setFunctionsListNameId(Integer functionsListNameId) {
		this.functionsListNameId = functionsListNameId;
	}

	@Column(name = "GROUP_FUNCTION_NAME")
	public String getFunctionsListName() {
		return functionsListName;
	}

	public void setFunctionsListName(String functionsListName) {
		this.functionsListName = functionsListName;
	}

	@Column(name = "GROUP_FUNCTION_DESCRIPTION")
	@Length(max = 1000)
	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Transient
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "functionsListNameId", cascade = CascadeType.ALL)
	public List<FunctionsListMiddle> getFunctionsListMiddle() {
		return functionsListMiddle;
	}

	public void setFunctionsListMiddle(List<FunctionsListMiddle> functionsListMiddle) {
		this.functionsListMiddle = functionsListMiddle;
	}
	
	public void addFunctionsListMiddle(FunctionsListMiddle f) {
		functionsListMiddle.add(f);
        f.setFunctionsListNameId(this);
    }

}
