package altalab.altacrm.model;

// Generated Nov 10, 2010 11:14:48 AM by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.Length;

@Entity
@Table(name = "group_functions")
public class GroupFunctions implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer groupFunctionId;
	private String groupFunctionName;
	private String description;
	private Boolean selected;
	private Set<TblGroupFunctions> tblGroupFunctions = new HashSet<TblGroupFunctions>(
			0);

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "GROUP_FUNCTION_ID", unique = true, nullable = false)
	public Integer getGroupFunctionId() {
		return groupFunctionId;
	}

	public void setGroupFunctionId(Integer groupFunctionId) {
		this.groupFunctionId = groupFunctionId;
	}

	@Column(name = "GROUP_FUNCTION_NAME")
	public String getGroupFunctionName() {
		return groupFunctionName;
	}

	public void setGroupFunctionName(String groupFunctionName) {
		this.groupFunctionName = groupFunctionName;
	}

	@Column(name = "GROUP_FUNCTION_DESCRIPTION")
	@Length(max = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Transient
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "groupFunctions")
	public Set<TblGroupFunctions> getTblGroupFunctions() {
		return tblGroupFunctions;
	}

	public void setTblGroupFunctions(Set<TblGroupFunctions> tblGroupFunctions) {
		this.tblGroupFunctions = tblGroupFunctions;
	}
}
