package altalab.altacrm.model;
// Generated Nov 10, 2010 11:14:48 AM by Hibernate Tools 3.2.4.GA

import java.util.Date;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import altalab.altacrm.consts.CommonValueConsts;

/**
 * 
 * <p> Class: TblUsers </p>
 * <p> Package: altalab.altacrm.model </p> 
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 * <p> Version: $1.0 </p>
 * <p> Create date: Jan 10, 2013 9:58:24 AM </p>
 * <p> Update date: Jan 10, 2013 9:58:24 AM </p>
 *
 */
@Entity
@Table(name = "tbl_users")
@AutoCreate
@Scope(ScopeType.SESSION)
@Name("tblUsers")
public class TblUsers implements java.io.Serializable {

	private static final long serialVersionUID = 6754694342191694000L;
	private Integer userId;
	private Integer groupId;
	private String userName;
	private String password;
	private String fullName;
	private String personalId;
	private String address;
	private String email;
	private String mobile;
	private String telephone;
	private Integer regUser;
	private Integer updUser;
	private Date regDate;
	private Date updDate;
	private Integer status;

	public TblUsers() {
	}

	public TblUsers(Integer groupId, String userName, String password,
			String fullName, String personalId, String address, String email,
			String mobile, String telephone, Integer regUser,
			Integer updUser, Date regDate, Date updDate) {
		this.groupId = groupId;
		this.userName = userName;
		this.password = password;
		this.fullName = fullName;
		this.personalId = personalId;
		this.address = address;
		this.email = email;
		this.mobile = mobile;
		this.telephone = telephone;
		this.regUser = regUser;
		this.updUser = updUser;
		this.regDate = regDate;
		this.updDate = updDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "USER_ID", unique = true, nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "GROUP_ID")
	public Integer getGroupId() {
		return this.groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	@Column(name = "USER_NAME", length = 100)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "PASSWORD", length = 100)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "FULL_NAME", length = 100)
	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Column(name = "PERSONAL_ID", length = 15)
	public String getPersonalId() {
		return this.personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	@Column(name = "ADDRESS", length = 1000)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "EMAIL", length = 100)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "MOBILE", length = 100)
	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "TELEPHONE", length = 100)
	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "REG_USER")
	public Integer getRegUser() {
		return this.regUser;
	}

	public void setRegUser(Integer regUser) {
		this.regUser = regUser;
	}

	@Column(name = "UPD_USER")
	public Integer getUpdUser() {
		return this.updUser;
	}

	public void setUpdUser(Integer updUser) {
		this.updUser = updUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REG_DATE", length = 19)
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPD_DATE", length = 19)
	public Date getUpdDate() {
		return this.updDate;
	}

	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	
	@Column(name = "STATUS")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@PrePersist
	public void init(){
		regUser = (Integer)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(CommonValueConsts.CURRENT_USER_ID_SESSION_NAME);
		regDate = new Date();
		updUser = regUser;
		updDate = regDate;
		setStatus(CommonValueConsts.STATUS_ACTIVE);
	}
	@PreUpdate
	public void preUpdate(){
		updUser = (Integer)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(CommonValueConsts.CURRENT_USER_ID_SESSION_NAME);
		updDate = new Date();
	}
	
}
