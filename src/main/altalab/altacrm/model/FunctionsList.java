package altalab.altacrm.model;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.Length;

/**
 * 
 * <p> Class: FunctionList </p>
 * <p> Package: altalab.altacrm.model </p> 
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 */
@Entity
@Table(name = "FUNCTION_LIST")
public class FunctionsList implements java.io.Serializable {

	
	private static final long serialVersionUID = 1L;
	private Integer functionId;
	private String functionCode;
	private String functionGroup;
	private String functionName;
	private String description;
	private Boolean selected;
	private Integer sortNumber;
	private Set<FunctionsListMiddle> functionsListMiddle = new HashSet<FunctionsListMiddle>(0);
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "FUNCTION_ID", unique = true, nullable = false)
	public Integer getFunctionId() {
		return this.functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	@Column(name = "FUNCTION_CODE", length = 10)
	@Length(max = 10)
	public String getFunctionCode() {
		return this.functionCode;
	}

	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	@Column(name = "FUNCTION_NAME", length = 100)
	@Length(max = 100)
	public String getFunctionName() {
		return this.functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	@Column(name = "DESCRIPTION")
	@Length(max = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name = "FUNCTION_GROUP")
	public String getFunctionGroup() {
		return functionGroup;
	}

	public void setFunctionGroup(String functionGroup) {
		this.functionGroup = functionGroup;
	}

	@Column(name = "SORTNUMBER", nullable = true)
	public Integer getSortNumber() {
		return sortNumber;
	}

	public void setSortNumber(Integer sortNumber) {
		this.sortNumber = sortNumber;
	}
	@Transient
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "functionsList")
	public Set<FunctionsListMiddle> getFunctionsListMiddle() {
		return functionsListMiddle;
	}

	public void setFunctionsListMiddle(Set<FunctionsListMiddle> functionsListMiddle) {
		this.functionsListMiddle = functionsListMiddle;
	}
	
	
}
