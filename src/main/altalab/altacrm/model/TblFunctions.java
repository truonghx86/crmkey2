package altalab.altacrm.model;
// Generated Nov 10, 2010 11:14:48 AM by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.Length;

/**
 * 
 * <p> Class: TblFunctions </p>
 * <p> Package: altalab.altacrm.model </p> 
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 * <p> Version: $1.0 </p>
 * <p> Create date: Feb 28, 2013 9:32:38 AM </p>
 * <p> Update date: Feb 28, 2013 9:32:38 AM </p>
 *
 */
@Entity
@Table(name = "tbl_functions")
public class TblFunctions implements java.io.Serializable {

	
	private static final long serialVersionUID = 1L;
	private Integer functionId;
	private String functionCode;
	private String functionGroup;
	private String functionName;
	private String description;
	private Boolean selected;
	private Set<TblGroupFunctions> tblGroupFunctions = new HashSet<TblGroupFunctions>(0);
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "FUNCTION_ID", unique = true, nullable = false)
	public Integer getFunctionId() {
		return this.functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	@Column(name = "FUNCTION_CODE", length = 10)
	@Length(max = 10)
	public String getFunctionCode() {
		return this.functionCode;
	}

	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	@Column(name = "FUNCTION_NAME", length = 100)
	@Length(max = 100)
	public String getFunctionName() {
		return this.functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	@Column(name = "DESCRIPTION")
	@Length(max = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name = "FUNCTION_GROUP")
	public String getFunctionGroup() {
		return functionGroup;
	}

	public void setFunctionGroup(String functionGroup) {
		this.functionGroup = functionGroup;
	}

	@Transient
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tblFunctions")
	public Set<TblGroupFunctions> getTblGroupFunctions() {
		return tblGroupFunctions;
	}

	public void setTblGroupFunctions(Set<TblGroupFunctions> tblGroupFunctions) {
		this.tblGroupFunctions = tblGroupFunctions;
	}

}
