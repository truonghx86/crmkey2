package altalab.altacrm.model;

// Generated Nov 10, 2010 11:14:48 AM by Hibernate Tools 3.2.4.GA

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.NotNull;

/**
 * 
 * <p>
 * Class: TblGroupFunctions
 * </p>
 * <p>
 * Package: altalab.altacrm.model
 * </p>
 * <p>
 * Author: truonghx
 * </p>
 * <p>
 * Update by: truonghx
 * </p>
 * <p>
 * Version: $1.0
 * </p>
 * <p>
 * Create date: Feb 28, 2013 9:36:12 AM
 * </p>
 * <p>
 * Update date: Feb 28, 2013 9:36:12 AM
 * </p>
 * 
 */
@Entity
@Table(name = "group_function_user")
public class GroupFunctionUser implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private GroupFunctionUserId id;
	private TblUsers tblUsers;
	private GroupFunctions groupFunction;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "groupFunctionId", column = @Column(name = "GROUP_FUNCTION_ID", nullable = false)),
			@AttributeOverride(name = "userId", column = @Column(name = "USER_ID", nullable = false)) })
	@NotNull
	public GroupFunctionUserId getId() {
		return id;
	}

	public void setId(GroupFunctionUserId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", nullable = false, insertable = false, updatable = false)
	@NotNull
	public TblUsers getTblUsers() {
		return tblUsers;
	}

	public void setTblUsers(TblUsers tblUsers) {
		this.tblUsers = tblUsers;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUP_FUNCTION_ID", nullable = false, insertable = false, updatable = false)
	@NotNull
	public GroupFunctions getGroupFunction() {
		return groupFunction;
	}

	public void setGroupFunction(GroupFunctions groupFunction) {
		this.groupFunction = groupFunction;
	}

}
