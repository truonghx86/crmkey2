package altalab.altacrm.model;

// Generated Nov 10, 2010 11:14:48 AM by Hibernate Tools 3.2.4.GA

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.NotNull;

/**
 * 
 * <p>
 * Class: TblGroupFunctions
 * </p>
 * <p>
 * Package: altalab.altacrm.model
 * </p>
 * <p>
 * Author: truonghx
 * </p>
 * <p>
 * Update by: truonghx
 * </p>
 * <p>
 * Version: $1.0
 * </p>
 * <p>
 * Create date: Feb 28, 2013 9:36:12 AM
 * </p>
 * <p>
 * Update date: Feb 28, 2013 9:36:12 AM
 * </p>
 * 
 */
@Entity
@Table(name = "tbl_group_functions")
public class TblGroupFunctions implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private GroupFunctionsId id;
	private TblFunctions tblFunctions;
	private GroupFunctions groupFunctions;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "groupFunctionId", column = @Column(name = "GROUP_FUNCTION_ID", nullable = false)),
			@AttributeOverride(name = "functionId", column = @Column(name = "FUNCTION_ID", nullable = false)) })
	@NotNull
	public GroupFunctionsId getId() {
		return id;
	}

	public void setId(GroupFunctionsId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FUNCTION_ID", nullable = false, insertable = false, updatable = false)
	@NotNull
	public TblFunctions getTblFunctions() {
		return tblFunctions;
	}

	public void setTblFunctions(TblFunctions tblFunctions) {
		this.tblFunctions = tblFunctions;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUP_FUNCTION_ID", nullable = false, insertable = false, updatable = false)
	@NotNull
	public GroupFunctions getGroupFunctions() {
		return groupFunctions;
	}

	public void setGroupFunctions(GroupFunctions groupFunctions) {
		this.groupFunctions = groupFunctions;
	}

}
