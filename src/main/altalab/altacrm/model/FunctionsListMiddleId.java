/**
 * <p> File: altalab.altacrm.model.GroupFunctionsId.java </p>
 * <p> Project: altacrmDemo </p>
 * <p> Copyright altalab. </p>
 * <p> All rights reserved. </p>
 * <p> Author: truonghx </p>
 * <p> Update by: truonghx </p>
 * <p> Version: $1.0 </p>
 * <p> Create date: Feb 28, 2013 9:48:54 AM </p>
 * <p> Update date: Feb 28, 2013 9:48:54 AM </p>
 **/

package altalab.altacrm.model;

import javax.persistence.Column;

public class FunctionsListMiddleId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer functionsListNameId;
	private Integer functionId;

	@Column(name = "FUNCTIONS_LIST_NAME_ID", nullable = false)
	public Integer getFunctionsListNameId() {
		return functionsListNameId;
	}

	public void setFunctionsListNameId(Integer functionsListNameId) {
		this.functionsListNameId = functionsListNameId;
	}

	@Column(name = "FUNCTION_ID", nullable = false)
	public Integer getFunctionId() {
		return this.functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}
}
